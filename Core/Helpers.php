<?php

/**
 * @param string $url
 */
function redirectTo( $url = '/' ) {
    header('Location: ' . $url);
    die();
}

/**
 * @param array $options
 * @param string $selectedOption
 */
function renderSelectOptions( $options, $selectedOption ) {
    foreach ( $options as $key => $value ) {
        $selectedAttr = $selectedOption === $key ? 'selected' : '';
        echo '<option ' . $selectedAttr . ' value="' . $key . '">' . $value . '</option>';
    }
}

/**
 * Get query string with selected page
 * @param int $page
 * @return string
 */
function getPaginateQuery( $page ) {
    $params = $_GET;
    $params['page'] = $page;

    return http_build_query($params);
}

/**
 * @param array $notifications List of notification strings
 */
function renderNotifications( $notifications ) {
    if( !empty( $notifications ) ) {
       foreach ( $notifications as $notification ) {
        ?>
            <div class="alert <?php echo $notification['type']; ?> alert-dismissible fade show" role="alert">
               <?php echo $notification['text']; ?>
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
            </div>
        <?php
       }
    }
}

/**
 * Check if user is logged in.
 * @return bool
 */
function isLoggedIn() {
    return isset( $_SESSION['auth'] );
}

/**
 * Stab for caps. Check if user has a capability
 * @param string $cap
 * @return bool
 */
function userHasCapability( $cap ) {
    return isLoggedIn();
}