<?php

class Model {
    protected $_table;
    protected $_fields;

    function __construct( $id = false ) {
        if( $id !== false ) {
            $this->getByID($id);
        }
    }

    public function getByID($id) {
        return $this->getByField('id', $id );
    }

    public function getByField( $fieldKey, $fieldValue ) {
        $modelData = Database::query('SELECT * FROM ' . $this->_table
            . ' WHERE ' . $fieldKey . '=\'' . $fieldValue . '\'');

        if( !$modelData ) {
            return false;
        }

        $modelData = $modelData->fetch_all(MYSQLI_ASSOC);

        if( empty( $modelData )) {
            return false;
        }

        $fields = array_shift( $modelData );
        foreach ( $fields as $key => $value ) {
            $this->$key = $value;
        }
        return true;
    }

    public function setField($field, $value) {
        $this->{$field} = $value;
        return $this;
    }

    //TODO: Generic implementation for any model.
    public function save() {
        return;
    }

    public function getAll() {
        return Database::queryAndFetch('SELECT * FROM ' . $this->_table);
    }

    public function countAll() {
        return array_shift(
            Database::queryAndFetch('SELECT COUNT(*) as count FROM ' . $this->_table)
        )['count'];
    }

    public function createFromArray( $array ) {
        foreach ( $this->_fields as $key ) {
            $this->$key = $array[$key];
        }

        return $this;
    }

}