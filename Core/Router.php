<?php

class Router {

    static function run() {
        $requested = Router::parseRequest();
        if( class_exists($requested['controller']) ) {
            $controller = new $requested['controller']();
        } else {
            Router::notFound();
            return;
        }


        if( method_exists( $requested['controller'], $requested['action'] ) ) {
            $controller->{$requested['action']}();
        } else {
            Router::notFound();
        }
    }

    static function parseRequest() {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $routes = explode('/', $url['path']);

        if ( !empty( $routes[1] ) ) {
            $requested['controller'] = $routes[1] . 'Controller';
        } else {
            $requested['controller'] = 'TaskController';
        }
        if ( !empty( $routes[2] ) ) {
            $requested['action'] = $routes[2];
        } else {
            $requested['action'] = 'index';
        }
        return $requested;
    }

    static function notFound() {
        echo 'not found';
    }
}