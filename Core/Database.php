<?php

class Database {
    private static $instance;
    private $MySQLi;

    private function __construct(array $dbConfig) {
        $this->MySQLi = new mysqli(
            $dbConfig['mysql_host'],
            $dbConfig['mysql_user'],
            $dbConfig['mysql_pass'],
            $dbConfig['mysql_db']
        );

        $this->MySQLi->set_charset("utf8");
    }

    public static function init( array $dbConfig ) {
        if ( self::$instance instanceof self ) {
            return false;
        }

        self::$instance = new self( $dbConfig );
        return true;
    }

    public static function getMySQLiObject() {
        return self::$instance->MySQLi;
    }

    public static function query( $q ) {
        return self::$instance
            ->MySQLi
            ->query($q);
    }

    public static function queryAndFetch( $q ) {
        return self::$instance
            ->MySQLi
            ->query($q)
            ->fetch_all( MYSQLI_ASSOC );
    }


    public static function esc( $str ) {
        return self::$instance->MySQLi->real_escape_string( htmlspecialchars($str) );
    }
}