<?php

class Controller {

    public function renderDefaultView( $template, $data = [] ) {
        extract($data);
        include 'Views/header.php';
        include 'Views/' . $template . '.php';
        include 'Views/footer.php';
    }
}