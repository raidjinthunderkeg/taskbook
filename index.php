<?php
require_once 'bootstrap.php';
session_start();

$dbConfig = parse_ini_file('config.ini');

Database::init($dbConfig);

Router::run();