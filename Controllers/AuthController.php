<?php

class AuthController extends Controller {

    public function index() {
        $this->renderDefaultView('login');
    }

    public function login() {
        $user = new UserModel();

        if( ! $user->getByName( $_POST['login'] ) ) {
            redirectTo('/auth/failed');
        } else {
            //No fancy stuff here, just simple flag to remind us that we are logged in.
            if( $_POST['password'] === $user->password ) {
                $_SESSION['auth'] = true;
                redirectTo('/');
            } else {
                redirectTo('/auth/failed');
            }
        }

    }

    public function logout() {
        session_destroy();
        redirectTo('/');
    }

    public function failed() {
        $this->renderDefaultView('login', [
            'notifications' => [
                [
                    'text' => 'Incorrect login or password.',
                    'type' => 'alert-danger'
                ]
            ]
        ]);
    }

}