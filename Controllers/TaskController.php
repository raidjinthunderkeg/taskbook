<?php

class TaskController extends Controller {
    private $TASKS_PER_PAGE = 3;

    public function index() {
        $task = new TaskModel();

        $page   = $_GET['page'] ? (int) $_GET['page'] : 1;

        $sortBy = empty( $_GET['sortBy'] ) ? 'default' : $_GET['sortBy'];
        $totalPages = ceil( $task->countAll() / $this->TASKS_PER_PAGE );

        $this->renderDefaultView('home', [
            'tasks'         => $task->query([
                'currentPage'   => $page,
                'sortBy'        => $sortBy,
            ]),
            'currentPage'   => $page,
            'totalPages'    => $totalPages,
            'notifications' => $this->getNotifications()
        ]);
    }

    public function create() {
        $task = new TaskModel();
        $task->createFromArray($_POST)->save();

        redirectTo('/?success=1');
    }

    public function update() {
        $task = new TaskModel( $_POST['id'] );

        if( !userHasCapability('edit') ) {
            redirectTo('/?not-allowed');
            return false;
        }

        $task
            ->setField('author',  $_POST['author'])
            ->setField('content', $_POST['content'])
            ->setField('email',   $_POST['email'])
            ->setField('status',  $_POST['status'] === 'on' ? 1 : 0)
            ->setField('edited', 1)
            ->save();

        redirectTo('/?updated');
    }

    private function getNotifications() {
        $notifications = [];

        if( isset( $_GET['success']) ) {
            $notifications[] = [
                'text' => 'Your task has been created. Now it is time to finish it!',
                'type' => 'alert-success'
            ];
        }
        if( isset( $_GET['updated']) ) {
            $notifications[] = [
                'text' => 'Task updated.',
                'type' => 'alert-success'
            ];
        }
        if( isset( $_GET['not-allowed']) ) {
            $notifications[] = [
                'text' => 'We dont like cheaters. May be next time.',
                'type' => 'alert-danger'
            ];
        }

        return $notifications;
    }
}