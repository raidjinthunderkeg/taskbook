<?php

class UserModel extends Model {
    protected $_table = 'users';
    protected $_fields = [
        'name', 'password',
    ];

    public function getByName($name) {
        return $this->getByField( 'name', $name );
    }
}