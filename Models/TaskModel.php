<?php

class TaskModel extends Model {
    protected $_table = 'tasks';
    protected $_fields = [
        'author', 'email', 'content', 'edited', 'status'
    ];

    public function query( $args = [] ) {
        $args = array_merge([
            'perPage'       => 3,
            'currentPage'   => 1,
            'sortBy'        => 'default'
        ], $args );

        $sortingMap = [
            'default'               => 'id DESC',
            'name_asc'              => 'author ASC',
            'name_desc'             => 'author DESC',
            'email_asc'             => 'email ASC',
            'email_desc'            => 'email DESC',
            'status_completed'      => 'status DESC',
            'status_noncompleted'   => 'status ASC',
        ];

        $orderBy = $sortingMap[ $args['sortBy'] ];
        $offset = $args['perPage'] * ( $args['currentPage'] - 1 );

        return Database::queryAndFetch('SELECT * FROM ' . $this->_table
            . ' ORDER BY ' . $orderBy
            . ' LIMIT ' . $args['perPage']
            . ' OFFSET ' . $offset
        );
    }

    public function save() {
        if( isset( $this->id ) ) {
            Database::query("UPDATE " . $this->_table. "
                SET author='" . $this->author .
                "', email='" . $this->email .
                "', content='" . $this->content .
                "', status='" . $this->status .
                "', edited='" . $this->edited .
                "' WHERE id=" . $this->id);
        } else {
            Database::query("INSERT INTO `tasks` (`id`, `author`, `email`, `status`, `content`) 
                VALUES (NULL, '"
                . Database::esc( $this->author )  . "', '"
                . Database::esc( $this->email )   . "', '"
                . Database::esc( $this->status )   . "', '"
                . Database::esc( $this->content ) . " . ')"
            );
        }
    }

}