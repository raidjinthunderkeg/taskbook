jQuery(document).ready( function () {

  var $sortSelect = $('#task-sort-by');
  var $sortForm   = $sortSelect.parent();
  var $createForm = $('#create-new-task');
  var $updateForm = $('#update-form');
  var $editButton = $('.js-edit-button');

  $sortSelect.change( function() {
    $sortForm.submit();
  });

  $createForm.validate();

  $editButton.click( function () {

    var id = $(this).attr('data-task-id');
    var status = $(this).attr('data-task-status') !== '0';
    var $task = $('#task-' + id);

    //Because this is jQuery.
    $updateForm.find('input[name="id"]')
      .val( id );
    $updateForm.find('input[name="author"]')
      .val( $task.find('.task-name').html() );
    $updateForm.find('textarea[name="content"]')
      .val( $task.find('.task-content').html().trim() );
    $updateForm.find('input[name="email"]')
      .val( $task.find('.task-email').html() );
    $updateForm.find('input[name="status"]')
      .prop('checked', !!status );
  });
});