<?php
require_once 'Core/Helpers.php';
require_once 'Controller.php';
require_once 'Core/Database.php';
require_once 'Core/Model.php';
require_once 'Core/Router.php';
require_once 'Core/View.php';

require_once 'Models/TaskModel.php';
require_once 'Models/UserModel.php';

require_once 'Controllers/AuthController.php';
require_once 'Controllers/TaskController.php';