<footer class="footer">
    <div class="container">
        <span class="text-muted">Made by <a href="https://t.me/selean_d">Selean</a>
            for <a href="https://beejee.org/">BeeJee</a></span>
    </div>
</footer>

<script type="text/javascript" src="libs/jquery.validate.min.js"></script>
<script type="text/javascript" src="script.js"></script>
</body>
</html>