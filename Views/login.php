<div class="container">

    <form action="/auth/login" method="post" class="login-form">
        <h3>Login into system </h3>
        <?php renderNotifications($notifications); ?>
        <div class="form-group">
            <label for="login">Your login</label>
            <input type="text" class="form-control" id="login" name="login" placeholder="Enter login">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
