<div class="container">


    <div class="page-title">
        <h3>Task list: </h3>
        <button type="button"
                class="btn btn-primary "
                data-toggle="modal"
                data-target="#create-task-modal"
        >
            Create new task
        </button>

        <div class="sort-controls">
           Sort By:
            <form id="create-new-task" method="get">

                <?php if( !empty( $_GET['page'] ) ) : ?>

                    <input name="page" type="hidden" value="<?php echo $_GET['page'] ?>">

                <?php endif; ?>

                <select class="sorting-select" id="task-sort-by" name="sortBy">
                    <?php
                        renderSelectOptions( [
                            'default'               => 'Default',
                            'name_asc'              => 'Name ASC',
                            'name_desc'             => 'Name DESC',
                            'email_asc'             => 'Email ASC',
                            'email_desc'            => 'Email DESC',
                            'status_completed'      => 'Completed tasks',
                            'status_noncompleted'   => 'Unfinished tasks'
                        ], $_GET['sortBy'] );
                    ?>
                </select>
            </form>
        </div>

        <?php renderNotifications( $notifications ); ?>

    </div>


    <div class="row">

        <?php foreach ( $tasks as $task ) : ?>

        <div class="col-sm">
            <div id="task-<?php echo $task['id']; ?>" class="card">
                <div class="card-header task-header
                    <?php if( $task['status'] == 1 ) echo 'alert-success'; ?>"
                >
                    <div>
                        <span class="task-name"><?php echo $task['author']; ?></span>
                        <?php if( $task['status'] == 1 ) echo '[Completed]'; ?>
                    </div>


                    <?php if( isLoggedIn() ) : ?>
                        <button type="button"
                                class="btn btn-primary js-edit-button"
                                data-toggle="modal"
                                data-target="#update-task-modal"
                                data-task-id="<?php echo $task['id']; ?>"
                                data-task-status="<?php echo $task['status']; ?>"
                        >
                            Edit
                        </button>
                    <?php endif; ?>

                </div>
                <div class="card-body">
                    <p class="card-text task-content">
                        <?php echo $task['content']; ?>
                    </p>
                    <span class="edited-tag">
                        <?php if( $task['edited'] == 1 ) echo 'Edited by admin'; ?>
                    </span>
                </div>
                <div class="card-footer text-muted task-email
                    <?php if( $task['status'] == 1) echo 'alert-success'; ?>"
                >
                    <?php echo $task['email']; ?>
                </div>
            </div>
        </div>

        <?php endforeach; ?>

    </div>


    <div class="modal fade" id="create-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="Task/create">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create new task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <input type="hidden" name="status" value="0">
                            <div class="form-group">
                                <label for="author">Name</label>
                                <input type="text"
                                       class="form-control"
                                       id="author"
                                       name="author"
                                       placeholder="John"
                                       required
                                >
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email"
                                       name="email"
                                       class="form-control"
                                       id="email"
                                       placeholder="name@example.com"
                                       required
                                >
                            </div>
                            <div class="form-group">
                                <label for="task-content">Task description</label>
                                <textarea class="form-control" id="task-content" rows="3" name="content" required></textarea>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Publish</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php if( isLoggedIn() ) : ?>

    <div class="modal fade" id="update-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="update-form" method="POST" action="Task/update">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="author">Name</label>
                            <input type="text"
                                   class="form-control"
                                   id="author"
                                   name="author"
                                   placeholder="John"
                                   required
                            >
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email"
                                   name="email"
                                   class="form-control"
                                   id="email"
                                   placeholder="name@example.com"
                                   required
                            >
                        </div>
                        <div class="form-group">
                            <label for="task-content">Task description</label>
                            <textarea class="form-control" id="task-content" rows="3" name="content" required></textarea>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" name="status" class="form-check-input" id="status">
                            <label class="form-check-label" for="status">Completed</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <div class="row pagination">
        <nav>
            <ul class="pagination">
                <li class="page-item <?php if( $currentPage-1 <= 0 ) echo 'disabled'; ?>">
                    <a class="page-link"
                       href="/?<?php echo getPaginateQuery( $currentPage-11 ); ?>"
                       aria-label="Previous"
                    >
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>

                <?php for ( $counter = 1; $counter <= $totalPages; $counter++ ) : ?>

                    <li class="page-item <?php if( $counter == $currentPage ) echo 'active'; ?>">
                        <a class="page-link" href="/?<?php echo getPaginateQuery( $counter ); ?>">
                            <?php echo $counter?>
                        </a>
                    </li>

                <?php endfor; ?>

                <li class="page-item <?php if( $currentPage+1 > $totalPages ) echo 'disabled'; ?>">
                    <a class="page-link"
                       href="/?<?php echo getPaginateQuery( $currentPage+1 ); ?>"
                       aria-label="Next"
                    >
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>


</div>